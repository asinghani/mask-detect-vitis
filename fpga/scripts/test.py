import cv2, sys

# Capture 20 frames and then save the last one

SOCKET = "/tmp/video-socket"
GSTPIPE = "shmsrc socket-path={} ! \
           video/x-raw, format=BGR, width=640, height=360, framerate=30/1 ! \
           decodebin ! videoconvert ! appsink".format(SOCKET)

cap = cv2.VideoCapture(GSTPIPE)

assert cap.isOpened()

for _ in range(20):
    frame = cap.read()[1]
    cv2.waitKey(10)

print("Saving frame as frame.png")
cv2.imwrite("frame.png", frame)