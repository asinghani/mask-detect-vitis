#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <dnndk/dnndk.h>
#include <math.h>
#include <signal.h>
#include <unistd.h>
#include <algorithm>
#include <array>
#include <atomic>
#include <chrono>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector> 
#include "dputils.h"

using namespace std;
using namespace cv;
using namespace std::chrono;

float thresh = 0.55;
float nms_thresh = 0.35;

uint32_t hashImg(Mat &img) {
    uint32_t sum = 0;

    for (int i = 0; i < img.rows; i++) {
        Vec3b pixel = img.at<Vec3b>(i, i);
        sum += pixel.val[0];
    }

    return sum;
}

void detectFaces(DPUTask *task, Mat &img) {
    DPUTensor *conv_in_tensor = dpuGetInputTensor(task, "L0");
    int inHeight = dpuGetTensorHeight(conv_in_tensor);
    int inWidth = dpuGetTensorWidth(conv_in_tensor);

    float scale_w = (float)img.cols / (float)inWidth;
    float scale_h = (float)img.rows / (float)inHeight;

    float imgMean[3];
    dpuGetKernelMean(task, imgMean, img.channels());
    dpuSetInputImageWithScale(task, "L0", img, imgMean, 1.0f, 0);
    dpuRunTask(task);
    DPUTensor *conv_out_tensor = dpuGetOutputTensor(task, "bb_output");
    int tensorSize = dpuGetTensorSize(conv_out_tensor);
    int outHeight = dpuGetTensorHeight(conv_out_tensor);
    int outWidth = dpuGetTensorWidth(conv_out_tensor); 
    vector<float> bb(tensorSize);
    int8_t *outAddr = (int8_t *)dpuGetOutputTensorAddress(task, "pixel_conv");
    int size = dpuGetOutputTensorSize(task, "pixel_conv");
    int channel = dpuGetOutputTensorChannel(task, "pixel_conv");
    float out_scale = dpuGetOutputTensorScale(task, "pixel_conv");
    float *softmax = new float[size]; 

    dpuGetOutputTensorInHWCFP32(task, "bb_output", bb.data(), tensorSize);
    dpuRunSoftmax(outAddr, softmax, channel, size/channel, out_scale);

    // Based on original densebox
    vector<vector<float>> boxes; 
    for (int i = 0; i < outHeight; i++) {
        for (int j = 0; j < outWidth; j++) {
            int position = i * outWidth + j;
            vector<float> box;
            if (softmax[position * 2 + 1] > thresh) {
                box.push_back(bb[position * 4 + 0] + j * 4);
                box.push_back(bb[position * 4 + 1] + i * 4);
                box.push_back(bb[position * 4 + 2] + j * 4);
                box.push_back(bb[position * 4 + 3] + i * 4);
                box.push_back(softmax[position * 2 + 1]);
                boxes.push_back(box);
            }
        }
    }
    cout << "img " << hashImg(img) << endl;

    vector<vector<float>> res = NMS(boxes, nms_thresh); 
    for (size_t i = 0; i < res.size(); ++i) { 
        float xmin = std::max(res[i][0] * scale_w, 0.0f);
        float ymin = std::max(res[i][1] * scale_h, 0.0f);
        float xmax = std::min(res[i][2] * scale_w, (float)img.cols);
        float ymax = std::min(res[i][3] * scale_h, (float)img.rows); 

        rectangle(img, Point(xmin, ymin), Point(xmax, ymax), Scalar(0, 255, 0), 1, 1, 0);
        cout << (int)xmin << " " << (int)xmax << " " << (int)ymin << " " << (int)ymax << endl;
    }
    cout << "-------------------------------" << endl;

    delete[] softmax;
}

int main(int argc, char **argv) {
    dpuOpen();
    DPUKernel *kernel = dpuLoadKernel("densebox");

    VideoCapture camera("shmsrc socket-path=/tmp/video-socket ! \
           video/x-raw, format=BGR, width=640, height=360, framerate=30/1 ! \
           decodebin ! videoconvert ! appsink"); 

    DPUTask *dputask = dpuCreateTask(kernel, 0);
    while (true) {
        Mat img;
        camera >> img;
        detectFaces(dputask, img);
        //waitKey(1);
    }
   
    dpuDestroyTask(dputask);
    dpuDestroyKernel(kernel);
    dpuClose();
    return 0;
}
