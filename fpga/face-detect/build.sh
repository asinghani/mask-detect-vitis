#!/bin/sh
mkdir -p build
g++ -c -O2 -Wall -Wpointer-arith -std=c++11 -ffast-math -Ilib -mcpu=cortex-a53 main.cc -o build/main.o
g++ -c -O2 -Wall -Wpointer-arith -std=c++11 -ffast-math -Ilib -mcpu=cortex-a53 lib/dputils.cpp -o build/dputils.o
g++ -O2 -Wall -Wpointer-arith -std=c++11 -ffast-math -Ilib -mcpu=cortex-a53 build/main.o build/dputils.o ../../models/dpu_facedetect.elf -o build/face_detect_gst -ln2cube -lhineon -lopencv_videoio  -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc -lopencv_core -lpthread
