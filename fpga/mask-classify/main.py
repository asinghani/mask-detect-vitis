import cv2, sys, time
import tflite_runtime.interpreter as tflite
import numpy as np
import threading
import cscore as cs

SOCKET = "/tmp/video-socket"
GST_PIPE = "shmsrc socket-path={} ! \
           video/x-raw, format=BGR, width=640, height=360, framerate=30/1 ! \
           decodebin ! videoconvert ! appsink".format(SOCKET)
CLASSES_NAME = ["with_mask", "without_mask", "mask_worn_incorrectly"]

STREAM_PORT = 1234

print("Starting...")
cap = cv2.VideoCapture(GST_PIPE)
print("Video opened...")

fifo = []
faces = []


cvSource = cs.CvSource("camSource", cs.VideoMode.PixelFormat.kMJPEG, 640, 360, 30)
server = cs.MjpegServer("mjpeg"+str(STREAM_PORT), STREAM_PORT)
server.setSource(cvSource)

assert cap.isOpened()

def img_hash(frame):
    return sum(frame[i, i, 0] for i in range(frame.shape[0])) % (2**32)

lock = threading.Lock()

def input_thread():
    global fifo, faces
    print("Started input")
    while True:
        x = input()
        assert x.split(" ")[0] == "img"
        h = int(x.split(" ")[1])
        dat = []
        while True:
            i = input()
            if "--------------" in i:
                break
            else:
                x0, x1, y0, y1 = map(int, map(float, i.split(" ")))
                dat.append((x0, x1, y0, y1))

        with lock:
            faces.append((h, dat))

            if len(faces) > 25:
                faces = faces[-25:]
            #print(faces)

    sys.exit(0)

def compute_thread():
    global fifo, faces
    while True:
        frame = cap.read()[1]
        fifo.append(frame)
        frame = fifo[0]
        h = img_hash(frame)

        out = None
        with lock:
            #print(len(faces))
            for x in faces:
                if x[0] == h:
                    out = x[1]
                    break

        if out is None:
            if len(fifo) > 4:
                fifo = fifo[1:]
                print("Dropped frame")
        else:
            #print("GOOD", h, out)
            fifo = fifo[1:]

            detections = []
            for (x0, x1, y0, y1) in out:
                width = x1 - x0
                height = y1 - y0

                assert width > 0 and height > 0

                y1 = int(y0 + (1.2 * width))
                detections.append((x0, x1, y0, y1))

            preds = classify_all(frame, detections)

            for pred, (x0, x1, y0, y1) in zip(preds, detections):
                if pred == 0 or pred == 2:
                    color = (0, 255, 0)
                else:
                    color = (0, 0, 255)

                cv2.rectangle(frame, (x0, y0), (x1, y1), color, 2)

            cv2.rectangle(frame, (5, 5), (635, 355), (255, 0, 0), 2)

            cvSource.putFrame(frame)


interpreter = tflite.Interpreter(model_path="../models/mask_classify.tflite")
interpreter.allocate_tensors()
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

def classify_all(frame, faces):
    #print("Classifying...")
    preds = []
    for face in faces:
        x0, x1, y0, y1 = face
        img = frame[y0:y1, x0:x1]
        #print(img.shape)
        img = cv2.resize(img, (32, 32)) / 255.0
        interpreter.set_tensor(input_details[0]["index"], img[None, :].astype(np.float32))

        start = time.time()
        interpreter.invoke()
        stop = time.time()
        #print("time", stop-start)

        output_data = interpreter.get_tensor(output_details[0]["index"])
        results = np.squeeze(output_data)

        print(np.round(results*100).astype(np.uint32))
        print(CLASSES_NAME[np.argmax(results)])
        preds.append(np.argmax(results))

    return preds

threading.Thread(target=compute_thread, args=[]).start()
input_thread()
sys.exit(0)
