#!/bin/bash

SRC=rtsp://192.168.1.10:8554/vid

modetest -M xlnx -w 39:g_alpha_en:0

### DECODE USING VCU
gst-launch-1.0 rtspsrc latency=200 location=$SRC ! \
		rtph264depay ! h264parse ! omxh264dec internal-entropy-buffers=3 ! \
		queue "max-size-buffers=30 leaky=2 min-threshold-buffers=10" name=vcu_buffer_0 ! \
                videoconvert ! video/x-raw, format=BGR, width=640, height=360, framerate=30/1 ! \
                queue ! \
	        shmsink socket-path=/tmp/video-socket sync=true wait-for-connection=false shm-size=10000000
modetest -M xlnx -w 39:g_alpha_en:1
