#!/bin/sh

gst-launch-1.0 v4l2src ! \
               videoconvert ! video/x-raw, format=BGR, width=640, height=360, framerate=30/1 ! \
               queue ! \
	       shmsink socket-path=/tmp/video-socket sync=true wait-for-connection=false shm-size=10000000
